#
# Cookbook Name: artifakt_app_magento
# Recipe: install
#

node[:deploy].each do |app_name, deploy|

  execute 'Install Magento' do
    cwd "#{deploy[:deploy_to]}/current/"
    command "php install.php --license_agreement_accepted 'yes' --locale 'en_US' --timezone 'America/Los_Angeles' --default_currency 'USD' --db_host '#{node[:db][:host]}' --db_name '#{node[:db][:name]}' --db_user '#{node[:db][:username]}' --db_pass '#{node[:db][:password]}' --db_prefix '#{node[:db][:prefix]}' --url 'http://#{node[:url][:elb].downcase}/' --use_rewrites 'yes' --use_secure 'no' --skip_url_validation 'yes' --secure_base_url 'https://#{node[:url][:elb].downcase}/' --use_secure_admin 'no' --admin_frontname '#{node[:app][:boprefix]}' --admin_firstname 'admin' --admin_lastname 'admin' --admin_email '#{node[:app][:user][:email]}' --admin_username '#{node[:app][:user][:username]}' --admin_password '#{node[:app][:user][:password]}' --encryption_key '#{node[:app][:secret]}'"
    only_if { node[:app][:data][:type] == 'without_sample' && File.exists?("#{deploy[:deploy_to]}/current/install.php") }
  end

end
