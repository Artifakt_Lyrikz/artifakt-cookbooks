#
# Cookbook Name: artifakt_crontab
# Recipe: setup
#

template "/var/www/crontabfile" do
    source "crontab.erb"
    mode 0770
    owner 'apache'
    variables(
        :crontab => (node[:stack][:crontab] rescue nil),
    )
end

bash "Setup crontab" do
    user "apache"
    cwd "/var/www"
    code <<-EOH
      crontab < crontabfile
    EOH
end