#
# Cookbook Name: artifakt_efs
# Recipe: mount
#

Chef::Log.info('Mount EFS Service')

package 'Install NFS' do
  case node[:platform]
  when 'ubuntu'
    package_name 'nfs-common'
  else
    package_name 'nfs-utils'
  end
end

directory node['efs']['path'] do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

execute "Mount EFS" do
    user "root"
    command "mount -t nfs4 -o nfsvers=4.1 #{node['efs']['domain']} #{node['efs']['path']}"
    not_if "grep -qs #{node['efs']['path']} /proc/mounts"
end
