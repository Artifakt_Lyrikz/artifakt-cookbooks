#
# Cookbook Name: artifakt_app_orocrm
# Recipe: install
#

node[:deploy].each do |app_name, deploy|

  execute 'Install Oro Crm' do
    cwd "#{deploy[:deploy_to]}/current/"
    command "php app/console oro:install --env prod --application-url http://#{node[:url][:elb].downcase}/ --organization-name OroCRM --user-firstname admin --user-lastname admin --user-name #{node[:app][:user][:username]} --user-email #{node[:app][:user][:email]} --user-password #{node[:app][:user][:password]} --sample-data n --drop-database --force -n"
    only_if { node[:app][:data][:type] == 'without_sample' }
  end
  
end
