#
# Cookbook Name: artifakt
# Recipe: setup
#

artifakt_custom_cookbooks do
    dir '/cookbooks/before_setup'
end

if node[:stack][:isScalable] == 'true'
    include_recipe 'artifakt_efs::mount'
end

if node[:elc][:host]
  bash "Install rediscli" do
    user "root"
    cwd "/tmp"
    code <<-EOH
      wget http://download.redis.io/redis-stable.tar.gz
      tar xvzf redis-stable.tar.gz
      cd redis-stable
      make
      cp src/redis-cli /usr/local/bin/
      chmod 755 /usr/local/bin/redis-cli
    EOH
  end
end rescue NoMethodError

include_recipe 'artifakt_htpasswd::setup'
include_recipe 'mod_php5_apache2'
include_recipe 'artifakt_crontab::install'
include_recipe 'artifakt_logs::install'
include_recipe 'ssh_users'
include_recipe 'artifakt_sftp_users'

template "/etc/php.ini" do
    source "php.ini.erb"
    mode 0777
    group "root"
    owner "root"
end

if node[:email][:type] == 'no_send'
    package 'sendmail' do
      action :remove
    end
end

include_recipe 'artifakt_composer::install'
include_recipe "artifakt_app_#{node[:app][:type]}::setup"

artifakt_custom_cookbooks do
    dir '/cookbooks/after_setup'
end
