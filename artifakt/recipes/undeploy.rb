#
# Cookbook Name: artifakt
# Recipe: undeploy
#

artifakt_custom_cookbooks do
    dir '/cookbooks/before_undeploy'
end

include_recipe 'deploy::php-undeploy'

artifakt_custom_cookbooks do
    dir '/cookbooks/before_undeploy'
end
