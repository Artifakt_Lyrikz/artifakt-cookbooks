#
# Cookbook Name: artifakt_efs
# Attributes: default
#

default[:efs][:path] = '/mnt/shared'
default[:efs][:domain] = "$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).#{node[:efs][:name]}.efs.#{node[:network][:region]}.amazonaws.com:/"
