#
# Cookbook Name: artifakt_app_magento2
# Definition: artifakt_magento2_install
#

define :artifakt_magento2_install  do
  release_path = node[:app_release_path]

  execute 'Save env.php' do
    cwd "#{release_path}/"
    user node[:app_user]
    group node[:app_group]
    command "mv app/etc/env.php app/etc/env.php.artifakt"
    only_if do
      File.exists?('/var/tmp/to_install.flag')
    end
  end

  execute 'Install Magento' do
    cwd "#{release_path}/"
    user node[:app_user]
    group node[:app_group]
    command "bin/magento setup:install --base-url=http://#{node[:url][:elb].downcase}/ --base-url-secure=https://#{node[:url][:elb].downcase}/  --db-host=#{node[:db][:host]} --db-name=#{node[:db][:name]} --db-user=#{node[:db][:username]} --db-password=#{node[:db][:password]} --admin-firstname=Magento --admin-lastname=User --admin-email=#{node[:app][:user][:email]} --admin-user=#{node[:app][:user][:username]} --admin-password=#{node[:app][:user][:password]} --language=en_US --currency=USD --timezone=America/Chicago --use-rewrites=1 --backend-frontname=#{node[:app][:boprefix]} --key=#{node[:app][:secret]}"
    only_if do
      File.exists?('/var/tmp/to_install.flag')
    end
  end

  execute 'Restore back env.php' do
    cwd "#{release_path}/"
    user node[:app_user]
    group node[:app_group]
    command "mv app/etc/env.php.artifakt app/etc/env.php"
    only_if do
      File.exists?('/var/tmp/to_install.flag')
    end
  end

end
