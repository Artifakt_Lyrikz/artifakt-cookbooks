#
# Cookbook Name: artifakt_app_akeneo
# Definition: artifakt_akeneo_deploy
#

define :artifakt_akeneo_deploy  do
  release_path = node[:app_release_path]
  env = "--env=#{node[:stack][:mode]}"

  execute "Dump assetic" do
      user 'root'
      cwd "#{release_path}/"
      command "php app/console assetic:dump #{env}"
      only_if { node[:app][:is_installed] == 'true' }
  end

  execute "Dump Oro assetic" do
      user node[:app_user]
      group node[:app_group]
      cwd "#{release_path}/"
      command "php app/console oro:assetic:dump #{env}"
      only_if { node[:app][:is_installed] == 'true' }
  end

  bash "Deploy data" do
      user node[:app_user]
      group node[:app_group]
      cwd "#{release_path}/"
      code <<-EOH
          php app/console #{env} oro:translation:dump en_US
          php app/console #{env} oro:translation:dump en
          php app/console #{env} oro:translation:dump fr_FR
          php app/console #{env} oro:translation:dump fr
      EOH
      only_if { node[:app][:is_installed] == 'true' }
  end

end
