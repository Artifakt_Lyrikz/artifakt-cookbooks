#
# Cookbook Name: artifakt
# Definition: artifakt_global_deploy
#

define :artifakt_global_deploy  do
  release_path = node[:app_release_path]
  app_type = node[:app][:type]
  env = "--env=#{node[:stack][:mode]}"
  custom_deploy = "artifakt_#{node[:app][:type]}_deploy"
  console_sf = "php app/console"
  if node[:app][:type] == 'symfony3'
    console_sf = "php bin/console"
  end

  if node[:app][:is_installed] == 'true'
    dateinstall = DateTime.now.to_date
  end

  if !File.file?("#{release_path}/#{node[app_type][:file_to_check]}")
    Chef::Application.fatal!("The code you are trying to deploy is not a #{app_type} application", 0)
  end

  bash "Set right permission to all files" do
    cwd "#{release_path}/"
    code <<-EOH
        find . -type f -exec chmod 775 {} \\;
        find . -type d -exec chmod 775 {} \\;
    EOH
  end

  execute 'Execute custom deploy script' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command 'sh artifakt/pre_deploy.sh'
    only_if { File.exist?("#{release_path}/artifakt/pre_deploy.sh") }
  end

  node[app_type][:shared_directories].each do |directory_to_mount|
    artifakt_mount_shared do
      dirname directory_to_mount
    end
  end

  node[app_type][:config_file].each do |source_file, path_file|
  	template "#{release_path}/#{path_file}" do
  	  source source_file
      cookbook "artifakt_app_#{app_type}"
  	  mode 0770
      owner node[:app_user]
      group node[:app_group]
  		variables(
  		  :dbhost =>        (node[:db][:host] rescue nil),
  		  :dbuser =>        (node[:db][:username] rescue nil),
  		  :dbpassword =>    (node[:db][:password] rescue nil),
  		  :dbname =>        (node[:db][:name] rescue nil),
        :dbprefix =>      (node[:db][:prefix] rescue nil),
        :cachehost =>     (node[:elc][:host] rescue nil),
        :appsecret =>     (node[:app][:secret] rescue nil),
        :appboprefix =>   (node[:app][:boprefix] rescue nil),
  		  :smtphost =>      (node[:email][:host] rescue nil),
  		  :smtplogin =>     (node[:email][:login] rescue nil),
  		  :smtppassword =>  (node[:email][:password] rescue nil),
        :dateinstall =>   (dateinstall rescue nil)
  		)
		end
  end

  execute 'Install fxp' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "composer require fxp/composer-asset-plugin:~1.3"
    only_if { node[:app][:type] == 'orocrm' || node[:app][:type] == 'orocommerce' || node[:app][:type] == 'marello' }
  end

  execute 'Install vendor' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "composer install"
    only_if { node[app_type][:composer_based] }
  end

  execute "Refresh assets" do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "#{console_sf} assets:install #{env}"
    only_if { node[app_type][:symfony_based] }
  end

  execute 'Update database' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "#{console_sf} do:sc:up --force"
    only_if { node[app_type][:symfony_based] && node[:app][:is_installed] == 'true' }
  end

  send("#{custom_deploy}")

  execute 'Execute custom deploy script' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command 'sh artifakt/deploy.sh'
    only_if { File.exist?("#{release_path}/artifakt/deploy.sh") }
  end

  template "#{release_path}/#{node[app_type][:document_root]}robots.txt" do
    owner node[:app_user]
    group node[:app_group]
    source 'robots.txt.erb'
    cookbook 'artifakt'
    only_if { node[:stack][:mode] == 'dev' && !File.exist?("#{release_path}/robots.txt")  }
  end

  node[app_type][:cached_directories].each do |directory_to_delete|
    execute 'Remove cache files' do
      cwd "#{release_path}/"
      command "rm -rf #{directory_to_delete}/*"
      only_if { node[:stack][:mode] == 'dev' }
    end
  end

  execute 'Warmup cache' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "#{console_sf} cache:warmup #{env}"
    only_if { node[app_type][:symfony_based] && node[:app][:is_installed] == 'true' }
  end

  execute 'Remove app_dev.php' do
    cwd "#{release_path}/web/"
    command "rm -f app_dev.php"
    only_if { node[app_type][:symfony_based] && node[:stack][:mode] == 'prod' }
  end
end
