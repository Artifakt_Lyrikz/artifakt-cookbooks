name        "artifakt_efs"
description "Artifakt EFS Recipes"
maintainer  "Artifakt"
license     "Apache 2.0"
version     "1.0.0"

recipe "artifakt_efs::mount", "Mount EFS hard drive"
recipe "artifakt_efs::umount", "Unmount EFS hard drive"
