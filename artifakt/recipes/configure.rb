#
# Cookbook Name: artifakt
# Recipe: configure
#

artifakt_custom_cookbooks do
    dir '/cookbooks/before_configure'
end

include_recipe 'php::configure'

artifakt_custom_cookbooks do
    dir '/cookbooks/after_configure'
end
