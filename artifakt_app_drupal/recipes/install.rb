#
# Cookbook Name: artifakt_app_drupal
# Recipe: install
#

node[:deploy].each do |app_name, deploy|

  execute 'Install Drupal' do
    user node[:apache][:user]
    cwd "#{deploy[:deploy_to]}/current/"
    command "drush site-install"
    only_if { node[:app][:data][:type] == 'without_sample' }
  end

end
