#
# Cookbook Name: artifakt_app_symfony
# Attributes: default
#

default[:symfony][:document_root] = 'web/'
default[:symfony][:composer_based] = true
default[:symfony][:symfony_based] = true
default[:symfony][:file_to_check] = 'app/AppKernel.php'
default[:symfony][:shared_directories] = [ 'app/uploads','web/uploads','app/logs' ]
default[:symfony][:cached_directories] = [ 'app/cache' ]
default[:symfony][:media_directories] = []
default[:symfony][:log_directories] = ['app/logs']
default[:symfony][:session_directories] = []
default[:symfony][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
