#
# Cookbook Name: artifakt_app_akeneo
# Attributes: default
#

default[:akeneo][:document_root] = 'web/'
default[:akeneo][:composer_based] = true
default[:akeneo][:symfony_based] = true
default[:akeneo][:file_to_check] = 'app/PimRequirements.php'
default[:akeneo][:shared_directories] = [ 'web/uploads','app/uploads','app/archive','app/import','app/export','web/import','web/export','app/file_storage','app/logs' ]
default[:akeneo][:cached_directories] = [ 'app/cache' ]
default[:akeneo][:media_directories] = []
default[:akeneo][:log_directories] = []
default[:akeneo][:session_directories] = []
default[:akeneo][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
