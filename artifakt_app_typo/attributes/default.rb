#
# Cookbook Name: artifakt_app_typo
# Attributes: default
#

default[:typo][:document_root] = ''
default[:typo][:composer_based] = true
default[:typo][:symfony_based] = false
default[:typo][:file_to_check] = 'index.php'
default[:typo][:shared_directories] = []
default[:typo][:cached_directories] = []
default[:typo][:media_directories] = []
default[:typo][:log_directories] = []
default[:typo][:session_directories] = []
default[:typo][:config_file] = {}
