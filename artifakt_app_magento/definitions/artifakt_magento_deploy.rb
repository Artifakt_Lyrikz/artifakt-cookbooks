#
# Cookbook Name: artifakt_app_magento
# Definition: artifakt_magento_deploy
#

define :artifakt_magento_deploy  do
  release_path = node[:app_release_path]

  execute "Remove local.xml is not installed" do
    cwd "#{release_path}/"
    command "rm -f app/etc/local.xml"
    only_if { node[:app][:is_installed] == 'false' }
  end

end
