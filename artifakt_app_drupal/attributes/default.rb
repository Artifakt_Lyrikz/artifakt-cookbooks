#
# Cookbook Name: artifakt_app_drupal
# Attributes: default
#

default[:drupal][:document_root] = ''
default[:drupal][:composer_based] = true
default[:drupal][:symfony_based] = false
default[:drupal][:file_to_check] = 'index.php'
default[:drupal][:shared_directories] = []
default[:drupal][:cached_directories] = []
default[:drupal][:config_file] = {'settings.php.erb' => 'sites/default/settings.php'}
