#
# Cookbook Name: artifakt_app_magento2
# Definition: artifakt_magento2_deploy
#

define :artifakt_magento2_deploy  do
  release_path = node[:app_release_path]

  execute 'Permission bin/magento' do
    cwd "#{release_path}/"
    command "chmod 777 bin/magento"
  end

  execute "Remove env.php is not installed" do
    cwd "#{release_path}/"
    command "rm -f app/etc/env.php"
    only_if { node[:app][:is_installed] == 'false' }
  end

  execute 'Setup config' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento module:enable --all --clear-static-content"
    only_if { !File.exists?("#{release_path}/app/etc/config.php") && node[:app][:is_installed] == 'true' }
  end

  execute 'Setup mode production' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento deploy:mode:set production -s"
    only_if { node[:stack][:mode] == 'prod' && node[:app][:is_installed] == 'true' }
  end

  execute 'Setup mode developer' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento deploy:mode:set developer -s"
    only_if { node[:stack][:mode] == 'dev' && node[:app][:is_installed] == 'true' }
  end

  execute 'Setup static' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento setup:static-content:deploy en_US fr_FR"
    only_if { node[:app][:is_installed] == 'true' }
  end

  execute 'Setup compile' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento setup:di:compile"
    only_if { node[:app][:is_installed] == 'true' }
  end

  execute 'Maintenance on' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento maintenance:enable"
    only_if { node[:app][:is_installed] == 'true' }
  end

  execute 'Setup upgrade' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento setup:upgrade --keep-generated"
    only_if { node[:app][:is_installed] == 'true' }
  end

  execute 'Maintenance off' do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    command "bin/magento maintenance:disable"
    only_if { node[:app][:is_installed] == 'true' }
  end

end