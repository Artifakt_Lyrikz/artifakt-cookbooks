#
# Cookbook Name: artifakt_app_akeneo
# Recipe: install
#

node[:deploy].each do |app_name, deploy|

  execute 'Install Akeneo' do
    cwd "#{deploy[:deploy_to]}/current/"
    command 'php app/console pim:install --force'
    only_if { node[:app][:data][:type] == 'without_sample' }
  end
end
