#
# Cookbook Name: artifakt_app_orocommerce
# Recipe: setup
#

php_version = node[:app][:language]

package 'nodejs' do
  retries 3
  retry_delay 5
  action :install
end

package 'tidy' do
  retries 3
  retry_delay 5
  action :install
end

package "php#{php_version}-tidy" do
  retries 3
  retry_delay 5
  action :install
end