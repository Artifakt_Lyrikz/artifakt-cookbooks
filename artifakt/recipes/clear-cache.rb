#
# Cookbook Name: artifakt
# Recipe: clear-cache
#

app_type = node[:app][:type]

node[app_type][:cached_directories].each do |directory_to_delete|
  execute 'Remove cache files' do
    user 'root'
    cwd "/mnt/shared/"
    command "rm -rf #{directory_to_delete}/*"
  end
end