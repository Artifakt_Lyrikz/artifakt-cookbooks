#
# Cookbook Name: artifakt_app_prestashop
# Attributes: default
#

default[:prestashop][:document_root] = ''
default[:prestashop][:composer_based] = true
default[:prestashop][:symfony_based] = false
default[:prestashop][:file_to_check] = 'index.php'
default[:prestashop][:shared_directories] = [ 'app/uploads','app/logs' ]
default[:prestashop][:cached_directories] = [ 'app/cache' ]
default[:prestashop][:media_directories] = []
default[:prestashop][:log_directories] = ['app/logs']
default[:prestashop][:session_directories] = []
default[:prestashop][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
