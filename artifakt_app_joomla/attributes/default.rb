#
# Cookbook Name: artifakt_app_joomla
# Attributes: default
#

default[:joomla][:document_root] = ''
default[:joomla][:composer_based] = true
default[:joomla][:symfony_based] = false
default[:joomla][:file_to_check] = 'index.php'
default[:joomla][:shared_directories] = []
default[:joomla][:cached_directories] = []
default[:joomla][:media_directories] = []
default[:joomla][:log_directories] = []
default[:joomla][:session_directories] = []
default[:joomla][:config_file] = {}
