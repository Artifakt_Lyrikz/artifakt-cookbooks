#
# Cookbook Name: artifakt
# Definition: artifakt_custom_cookbooks
#

define :artifakt_custom_cookbooks, :dir => nil do
  directory = params[:dir]

  if !Dir.exist?(directory)
    Chef::Log.info("Skipping run_custom_cookbooks because directory #{directory} doesn't exist")
    next
  end

  Dir.foreach(directory) do |item|
    next if item == '.' or item == '..'
    execute "./#{item}" do
        user "root"
        cwd directory
    end
  end
end
