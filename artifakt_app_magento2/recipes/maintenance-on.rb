#
# Cookbook Name: artifakt_app_magento
# Recipe: maintenance-on
#

node[:deploy].each do |app_name, deploy|
  bash "maintenance_on" do
    user "root"
    cwd "#{deploy[:deploy_to]}/current/"
    code <<-EOH
      php bin/magento maintenance:enable
    EOH
  end
end
