#
# Cookbook Name: artifakt
# Recipe: deploy
#

artifakt_custom_cookbooks do
    dir '/cookbooks/before_deploy'
end

include_recipe 'deploy::php'

artifakt_custom_cookbooks do
    dir '/cookbooks/after_deploy'
end
