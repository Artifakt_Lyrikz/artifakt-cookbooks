#
# Cookbook Name: artifakt_app_magento2
# Attributes: default
#

default[:magento2][:document_root] = ''
default[:magento2][:composer_based] = true
default[:magento2][:symfony_based] = false
default[:magento2][:file_to_check] = 'app/etc/di.xml'
default[:magento2][:shared_directories] = [ 'pub/media','var' ]
default[:magento2][:cached_directories] = [ 'var/cache','var/page_cache' ]
default[:magento2][:media_directories] = ['media/catalog/product/cache']
default[:magento2][:log_directories] = ['var/log','var/report']
default[:magento2][:session_directories] = ['var/session']
default[:magento2][:config_file] = {'env.php.erb' => 'app/etc/env.php'}
