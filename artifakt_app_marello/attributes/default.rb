#
# Cookbook Name: artifakt_app_marello
# Attributes: default
#

default[:marello][:document_root] = 'web/'
default[:marello][:composer_based] = true
default[:marello][:symfony_based] = true
default[:marello][:file_to_check] = 'app/oro-check.php'
default[:marello][:shared_directories] = [ 'app/uploads','web/uploads','app/logs' ]
default[:marello][:cached_directories] = [ 'app/cache' ]
default[:marello][:media_directories] = []
default[:marello][:log_directories] = []
default[:marello][:session_directories] = []
default[:marello][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
