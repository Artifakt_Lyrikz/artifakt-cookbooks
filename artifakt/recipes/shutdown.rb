#
# Cookbook Name: artifakt
# Recipe: shutdown
#

artifakt_custom_cookbooks do
    dir '/cookbooks/before_configure'
end

if node[:stack][:isScalable] == 'true'
    include_recipe 'artifakt_efs::umount'
end

include_recipe 'apache2::stop'

artifakt_custom_cookbooks do
    dir '/cookbooks/after_configure'
end
