#
# Cookbook Name: artifakt_app_magento2
# Recipe: setup
#

bash "Install N98" do
  user "root"
  code <<-EOH
		  wget https://files.magerun.net/n98-magerun2.phar
      mv n98-magerun2.phar /usr/local/bin/n98-magerun2
      chmod +x /usr/local/bin/n98-magerun2
  EOH
end
