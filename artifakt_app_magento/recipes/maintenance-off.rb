#
# Cookbook Name: artifakt_app_magento
# Recipe: maintenance-off
#

node[:deploy].each do |app_name, deploy|
  bash "maintenance_on" do
    user "root"
    cwd "#{deploy[:deploy_to]}/current/"
    code <<-EOH
      rm -rf maintenance.flag
    EOH
  end
end
