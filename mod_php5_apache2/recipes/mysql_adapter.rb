package 'mysql' do
  package_name value_for_platform_family(
     'rhel' => 'mysql',
     'debian' => 'mysql-client'
  )
  retries 3
  retry_delay 5
end

package 'php-mysql' do
  package_name value_for_platform_family(
    'rhel' => "php#{node[:app][:language]}-mysqlnd",
    'debian' => 'php5-mysql'
  )
  retries 3
  retry_delay 5
end

if node[:stack][:type] == 'standard'
  package 'mysql-server' do
    retries 3
    retry_delay 5
  end

  template "/var/tmp/init_mysql.sql" do
		source 'init_mysql.sql.erb'
    cookbook "mod_php5_apache2"
		mode 0755
    owner 'root'
    group 'root'
		variables(
		  :dbuser =>        (node[:db][:username] rescue nil),
		  :dbpassword =>    (node[:db][:password] rescue nil),
		  :dbname =>        (node[:db][:name] rescue nil)
		)
  end

  execute 'Start mysql server' do
    user 'root'
    command 'service mysqld start'
  end
  
  execute 'Init mysql' do
    user 'root'
    command "mysql < /var/tmp/init_mysql.sql"
  end
end
