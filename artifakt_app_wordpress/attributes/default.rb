#
# Cookbook Name: artifakt_app_wordpress
# Attributes: default
#

default[:wordpress][:document_root] = ''
default[:wordpress][:composer_based] = false
default[:wordpress][:symfony_based] = false
default[:wordpress][:file_to_check] = 'wp-settings.php'
default[:wordpress][:shared_directories] = [ 'wp-content/cache','wp-content/uploads','wp-content/backups','wp-content/backup-db','wp-content/upgrade' ]
default[:wordpress][:cached_directories] = [ 'wp-content/cache' ]
default[:wordpress][:media_directories] = []
default[:wordpress][:log_directories] = []
default[:wordpress][:session_directories] = []
default[:wordpress][:config_file] = {'wp-config.php.erb' => 'wp-config.php'}
