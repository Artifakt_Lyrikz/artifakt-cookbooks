#
# Cookbook Name: artifakt_app_wordpress
# Recipe: install
#

node[:deploy].each do |app_name, deploy|

  execute 'Install Wordpress' do
    user node[:apache][:user]
    cwd "#{deploy[:deploy_to]}/current/"
    command "wp core install --url=#{node[:url][:elb].downcase} --title=Example --admin_user=#{node[:app][:user][:username]} --admin_password=#{node[:app][:user][:password]} --admin_email=#{node[:app][:user][:email]}"
    only_if { node[:app][:data][:type] == 'without_sample' }
  end

end

