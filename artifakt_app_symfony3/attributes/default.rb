#
# Cookbook Name: artifakt_app_symfony3
# Attributes: default
#

default[:symfony3][:document_root] = 'web/'
default[:symfony3][:composer_based] = true
default[:symfony3][:symfony_based] = true
default[:symfony3][:file_to_check] = 'app/AppKernel.php'
default[:symfony3][:shared_directories] = [ 'app/uploads','web/uploads','var/logs' ]
default[:symfony3][:cached_directories] = [ 'var/cache' ]
default[:symfony3][:media_directories] = []
default[:symfony3][:log_directories] = ['var/logs']
default[:symfony3][:session_directories] = []
default[:symfony3][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
