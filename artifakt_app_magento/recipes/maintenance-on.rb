#
# Cookbook Name: artifakt_app_magento
# Recipe: maintenance-on
#

node[:deploy].each do |app_name, deploy|
  bash "maintenance_on" do
    user "root"
    cwd "#{deploy[:deploy_to]}/current/"
    code <<-EOH
      echo '' > maintenance.flag
      chown -R apache:opsworks maintenance.flag
    EOH
  end
end
