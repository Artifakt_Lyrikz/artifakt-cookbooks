#
# Cookbook Name: artifakt_app_orocommerce
# Attributes: default
#

default[:orocommerce][:document_root] = 'web/'
default[:orocommerce][:composer_based] = true
default[:orocommerce][:symfony_based] = true
default[:orocommerce][:file_to_check] = 'app/oro-check.php'
default[:orocommerce][:shared_directories] = [ 'app/uploads','web/uploads','app/logs' ]
default[:orocommerce][:cached_directories] = [ 'app/cache' ]
default[:orocommerce][:media_directories] = []
default[:orocommerce][:log_directories] = ['app/logs']
default[:orocommerce][:session_directories] = []
default[:orocommerce][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
