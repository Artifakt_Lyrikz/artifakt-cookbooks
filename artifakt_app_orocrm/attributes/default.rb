#
# Cookbook Name: artifakt_app_orocrm
# Attributes: default
#

default[:orocrm][:document_root] = 'web/'
default[:orocrm][:composer_based] = true
default[:orocrm][:symfony_based] = true
default[:orocrm][:file_to_check] = 'app/oro-check.php'
default[:orocrm][:shared_directories] = [ 'app/uploads','web/uploads','app/logs' ]
default[:orocrm][:cached_directories] = [ 'app/cache' ]
default[:orocrm][:media_directories] = []
default[:orocrm][:log_directories] = []
default[:orocrm][:session_directories] = []
default[:orocrm][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
