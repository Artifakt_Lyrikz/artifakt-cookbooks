#
# Cookbook Name: artifakt_app_satis
# Attributes: default
#

default[:satis][:document_root] = 'web/'
default[:satis][:composer_based] = true
default[:satis][:symfony_based] = false
default[:satis][:file_to_check] = 'src/Composer/Satis/Satis.php'
default[:satis][:shared_directories] = []
default[:satis][:cached_directories] = []
default[:satis][:media_directories] = []
default[:satis][:log_directories] = []
default[:satis][:session_directories] = []
default[:satis][:config_file] = {}
