#
# Cookbook Name: artifakt_logs
# Recipe: install
#

directory "/etc/awslogs" do
  recursive true
end

template "/etc/awslogs/cwlogs.cfg" do
  cookbook "artifakt_logs"
  source "cwlogs.cfg.erb"
  owner "root"
  group "root"
  mode 0644
end

directory "/opt/aws/cloudwatch" do
  recursive true
end

remote_file "/opt/aws/cloudwatch/awslogs-agent-setup.py" do
  source "https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py"
  mode "0755"
end

execute "Install CloudWatch Logs agent" do
  command "/opt/aws/cloudwatch/awslogs-agent-setup.py -n -r us-east-1 -c /etc/awslogs/cwlogs.cfg"
  not_if { system "pgrep -f aws-logs-agent-setup" }
end
