#
# Cookbook Name: artifakt_composer
# Recipe: install
#

bash "install_composer" do
    user "root"
    code <<-EOH
      php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
      php composer-setup.php
      php -r "unlink('composer-setup.php');"
      mv composer.phar /usr/local/bin/composer
    EOH
end

bash "add apache composer cache" do
    user "root"
    code <<-EOH
      mkdir -p /var/www/.composer
      chown -R apache:opsworks /var/www/.composer
      chmod -R 777 /var/www/.composer
    EOH
end