#
# Cookbook Name: artifakt_app_orocrm
# Definition: artifakt_orocrm_deploy
#

define :artifakt_orocrm_deploy  do
  release_path = node[:app_release_path]

  bash "Refresh assets" do
    user node[:app_user]
    group node[:app_group]
    cwd "#{release_path}/"
    code <<-EOH
        php app/console assets:install --env=prod
        php app/console fos:js-routing:dump --target=web/js/routes.js 
        php app/console assetic:dump --env=prod
        php app/console oro:localization:dump --env=prod
    EOH
  end
end
