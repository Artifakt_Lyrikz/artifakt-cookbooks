#
# Cookbook Name: artifakt_efs
# Recipe: umount
#

execute "Unmount EFS" do
    user "root"
    command "umount #{node['efs']['path']}"
end

directory node['efs']['path'] do
  recursive true
  action :delete
end
