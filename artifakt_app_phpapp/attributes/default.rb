#
# Cookbook Name: artifakt_app_phpapp
# Attributes: default
#

default[:phpapp][:document_root] = ''
default[:phpapp][:composer_based] = false
default[:phpapp][:symfony_based] = false
default[:phpapp][:file_to_check] = 'index.php'
default[:phpapp][:shared_directories] = []
default[:phpapp][:cached_directories] = []
default[:phpapp][:media_directories] = []
default[:phpapp][:log_directories] = []
default[:phpapp][:session_directories] = []
default[:phpapp][:config_file] = {}
